import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:tanzania_locator/api/config.dart';




//get all zones
Future<List<dynamic>> getZones() async{
  try {
    final response = await http.get(BASE_URL + 'zones');
    if (response.statusCode == 200){
      print(json.decode(response.body)['result']['zones'][0]['name']);
      return json.decode(response.body)['result']['zones'];
    }
    else
      throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');
  }
  catch (SocketException){
    throw PlatformException(code: 'CONNECTIVITY_ERROR', message: 'There seems to be a problem with the internet connection');
  }

}

Future<Map<String, dynamic>> getZone(int zoneId) async{
  final response = await http.get(BASE_URL + 'zones/$zoneId');
  if (response.statusCode == 200)
    return json.decode(response.body)['result']['zone'];
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');

}

Future<Map<String, dynamic>> searchZone(String zoneName) async{
  final response = await http.get(BASE_URL + 'zones/search/$zoneName');
  if (response.statusCode == 200)
    return json.decode(response.body)['result'];
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');

}

Future<List<dynamic>> getRegions() async{
  final response = await http.get(BASE_URL + 'regions');
  if (response.statusCode == 200){
    print(json.decode(response.body)['result']['regions'][0]['name']);
    return json.decode(response.body)['result']['regions'];
  }
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');
}

Future<Map<String, dynamic>> getRegion(int regionId) async{
  final response = await http.get(BASE_URL + 'regions/$regionId');
  if (response.statusCode == 200)
    return json.decode(response.body)['result']['region'];
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');

}

Future<Map<String, dynamic>> searchRegion(String regionName) async{
  final response = await http.get(BASE_URL + 'regions/search/$regionName');
  if (response.statusCode == 200)
    return json.decode(response.body)['result'];
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');

}

Future<List<dynamic>> getDistricts() async{
  final response = await http.get(BASE_URL + 'districts');
  if (response.statusCode == 200){
    print(json.decode(response.body)['result']['districts'][0]['name']);
    return json.decode(response.body)['result']['districts'];
  }
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');
}

Future<Map<String, dynamic>> getDistrict(int districtId) async{
  final response = await http.get(BASE_URL + 'districts/$districtId');
  if (response.statusCode == 200)
    return json.decode(response.body)['result']['district'];
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');

}

Future<Map<String, dynamic>> searchDistrict(String districtName) async{
  final response = await http.get(BASE_URL + 'districts/search/$districtName');
  if (response.statusCode == 200)
    return json.decode(response.body)['result'];
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');

}

Future<List<dynamic>> getWards() async{
  final response = await http.get(BASE_URL + 'wards');
  if (response.statusCode == 200){
    print(json.decode(response.body)['result']['wards'][0]['name']);
    return json.decode(response.body)['result']['wards'];
  }
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');
}

Future<Map<String, dynamic>> getWard(int wardId) async{
  final response = await http.get(BASE_URL + 'wards/$wardId');
  if (response.statusCode == 200)
    return json.decode(response.body)['result']['ward'];
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');

}

Future<Map<String, dynamic>> searchWard(String wardName) async{
  final response = await http.get(BASE_URL + 'wards/search/$wardName');
  if (response.statusCode == 200)
    return json.decode(response.body)['result'];
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');

}


Future<Map<String, dynamic>> getVillage(int villageId) async{
  final response = await http.get(BASE_URL + 'villages/$villageId');
  if (response.statusCode == 200)
    return json.decode(response.body)['result']['village'];
  else
    throw PlatformException(code: response.statusCode.toString(), message: 'Error fetching data');

}






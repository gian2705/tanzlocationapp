import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tanzania_locator/api/api.dart';
import 'package:tanzania_locator/ui/village.dart';

class VillagesPage extends StatefulWidget {
  final List<dynamic> villages;
  final String title;

  VillagesPage({this.villages, @required this.title});

  @override
  _VillagesPageState createState() => _VillagesPageState();
}

class _VillagesPageState extends State<VillagesPage> {
  bool _isLoading = false;
  @override
  Widget build(BuildContext context) {
    List<dynamic> _villages = widget.villages;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Container(
        child: Center(
          child: _isLoading ? CupertinoActivityIndicator(): ListView.separated(
              itemBuilder: (context, index) {
                return ListTile(
                  leading: Icon(Icons.location_on),
                  title: Text(_villages[index]['name']),
                  trailing: Icon(Icons.arrow_forward_ios),
                  onTap: () {
                    setState(() {
                      _isLoading = true;
                    });
                   _openVillagePage(_villages[index]);
                  },
                );
              },
              separatorBuilder: (context, index) => Divider(),
              itemCount: _villages.length),
        ),
      ),
    );
  }

  void _openVillagePage(village) {
    getVillage(village['id']).then((value){
      setState(() {
        _isLoading = false;
      });

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => Village(
                title: value['details']['name'],
                village: value,
              )));
    });
  }
}

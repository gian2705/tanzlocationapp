import 'package:flutter/material.dart';

class Village extends StatefulWidget {
  Village({Key key, this.title, this.village}) : super(key: key);

  static const String routeName = "/Village";

  final String title;
  final village;

  @override
  _VillageState createState() => new _VillageState();
}

class _VillageState extends State<Village> {
  TextStyle subtitleStyle =
      new TextStyle(fontSize: 22, fontWeight: FontWeight.bold);
  TextStyle contentStyle = new TextStyle(
    fontSize: 20,
  );

  @override
  Widget build(BuildContext context) {
    var _village = widget.village;
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Container(
        child: Center(
          child: ListView(
            padding: EdgeInsets.all(10.0),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(30.0),
              ),
              Center(
                child: Text(
                  _village['details']['name'],
                  style: TextStyle(fontSize: 30),
                ),
              ),
              Padding(padding: EdgeInsets.all(20)),
              Divider(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 30.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            'Zone: ',
                            style: subtitleStyle,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:40.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            _village['zone']['name'],
                            style: contentStyle,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            'Region: ',
                            style: subtitleStyle,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:40.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            _village['region']['name'],
                            style: contentStyle,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right:10.0),
                      child: Column(
                        children: <Widget>[
                          Text('District: ', style: subtitleStyle),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            _village['district']['name'],
                            style: contentStyle,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 30.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            'Ward: ',
                            style: subtitleStyle,
                          ),
                        ],
                ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            _village['ward']['name'],
                            style: contentStyle,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Divider(),
              Padding(padding: EdgeInsets.all(20.0)),
              MaterialButton(
                onPressed: null,
                child: Text('View on Map'),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onFloatingActionButtonPressed() {}
}

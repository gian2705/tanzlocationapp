import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tanzania_locator/api/api.dart';
import 'package:tanzania_locator/example_data/examples.dart';
import 'package:tanzania_locator/ui/villages.dart';
import 'package:connectivity/connectivity.dart';
import 'package:toast/toast.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  static const String routeName = "/Home";

  final String title;

  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new LocationSelector(),
    );
  }
}

class LocationSelector extends StatefulWidget {
  @override
  _LocationSelectorState createState() => _LocationSelectorState();
}

class _LocationSelectorState extends State<LocationSelector> {
  List<dynamic> _zones, _regions, _wards, _districts, _village;
  bool _isLoading = false, _noInternet = false;
  var _selectedZone, _selectedRegion, _selectedDistrict, _selectedWard;
  final Connectivity _connectivity = new Connectivity();

  @override
  void initState() {
    ConnectivityResult connectivityResult;

    _connectivity.checkConnectivity().then((value) {
      connectivityResult = value;
    }).then((value) {
      if (connectivityResult == ConnectivityResult.none){
        setState(() {
          _noInternet = true;
        });
      }
      else {
        setState(() {
          _isLoading = true;
        });
        getZones().then((value) {
          setState(() {
            _zones = value;
            _isLoading = false;
          });
        }).catchError((error) {
          setState(() {
            _isLoading = false;
          });
          Toast.show(error.message, context, duration: 4);
        });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: _noInternet
            ? Text(
                'No internet connection',
                style: TextStyle(color: Colors.grey),
              )
            : _isLoading
                ? CupertinoActivityIndicator()
                : ListView(
                    padding: EdgeInsets.all(20.0),
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(20),
                          ),
                          Text('Zone',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold)),
                          DropdownButtonFormField(
                            decoration: InputDecoration(icon: Icon(Icons.map)),
                            items: _zones == null ? null : _zones.map((value) {
                              return DropdownMenuItem(
                                child: Text(value['name']),
                                value: value,
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _selectedZone = value;
                                _getRegions();
                                _isLoading = true;
                              });
                            },
                            value: _selectedZone,
                          ),
                          Padding(
                            padding: EdgeInsets.all(20),
                          ),
                          Text('Region',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold)),
                          DropdownButtonFormField(
                              value: _selectedRegion,
                              decoration:
                                  InputDecoration(icon: Icon(Icons.map)),
                              items: _regions == null
                                  ? null
                                  : _regions.map((value) {
                                      return DropdownMenuItem(
                                          child: Text(value['name']),
                                          value: value);
                                    }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  _selectedRegion = value;
                                  _getDistricts();
                                });
                              }),
                          Padding(
                            padding: EdgeInsets.all(20),
                          ),
                          Text('District',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold)),
                          DropdownButtonFormField(
                              value: _selectedDistrict,
                              decoration:
                                  InputDecoration(icon: Icon(Icons.map)),
                              items: _districts == null
                                  ? null
                                  : _districts.map((value) {
                                      return DropdownMenuItem(
                                        child: Text(value['name']),
                                        value: value,
                                      );
                                    }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  _selectedDistrict = value;
                                  _isLoading = true;
                                  _getWards();
                                });
                              }),
                          Padding(
                            padding: EdgeInsets.all(20),
                          ),
                          Text('Ward',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold)),
                          DropdownButtonFormField(
                              value: _selectedWard,
                              decoration:
                                  InputDecoration(icon: Icon(Icons.map)),
                              items: _wards == null
                                  ? null
                                  : _wards.map((value) {
                                      return DropdownMenuItem(
                                        child: Text(value['name']),
                                        value: value,
                                      );
                                    }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  _selectedWard = value;
                                  _isLoading = true;
                                  _showVillages();
                                });
                              }),
                        ],
                      )
                    ],
                  ),
      ),
    );
  }

  void _getRegions() {
    getZone(_selectedZone['id']).then((value) {
      setState(() {
        _selectedRegion = null;
        _selectedWard = null;
        _selectedDistrict = null;
        _regions = value['regions'];
        _isLoading = false;
      });
    }).catchError((error) {
      setState(() {
        _isLoading = false;
      });
      Toast.show(error.message, context, duration: 4);
    });
  }

  void _getWards() {
    getDistrict(_selectedDistrict['id']).then((value) {
      setState(() {
        _selectedWard = null;
        _wards = value['wards'];
        _isLoading = false;
      });
    }).catchError((error) {
      setState(() {
        _isLoading = false;
      });
      Toast.show(error.message, context, duration: 4);
    });
  }

  void _getDistricts() {
    getRegion(_selectedRegion['id']).then((value) {
      setState(() {
        _selectedDistrict = null;
        _selectedWard = null;
        _districts = value['districts'];
        _isLoading = false;
      });
    }).catchError((error) {
      setState(() {
        _isLoading = false;
      });
      Toast.show(error.message, context, duration: 4);
    });
  }

  void _showVillages() {
    getWard(_selectedWard['id']).then((value) {
      setState(() {
        _isLoading = false;
      });
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => VillagesPage(
                    title: _selectedWard['name'],
                    villages: value['villages'],
                  )));
    }).catchError((error) {
      setState(() {
        _isLoading = false;
      });
      Toast.show(error.message, context, duration: 4);
    });
  }
}

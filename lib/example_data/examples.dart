Map<String, dynamic>ZONES = {
  "status": "ok",
  "code": 200,
  "messages": [],
  "result": {
    "zones": [
      {
        "id": 1,
        "name": "Central"
      },
      {
        "id": 2,
        "name": "Coastal"
      },
      {
        "id": 3,
        "name": "Lake"
      },
      {
        "id": 4,
        "name": "Northen"
      },
      {
        "id": 5,
        "name": "Southern"
      },
      {
        "id": 6,
        "name": "Southern Highland"
      }
    ]
  }
};
Map<String, dynamic> REGIONS = {
  "status": "ok",
  "code": 200,
  "messages": [],
  "result": {
    "regions": [
      {
        "id": 1,
        "name": "Arusha",
        "zone": 4
      },
      {
        "id": 2,
        "name": "Dar es Salaam",
        "zone": 2
      },
      {
        "id": 3,
        "name": "Dodoma",
        "zone": 1
      },
      {
        "id": 4,
        "name": "Geita",
        "zone": 3
      },
      {
        "id": 5,
        "name": "Iringa",
        "zone": 6
      },
      {
        "id": 6,
        "name": "Kagera",
        "zone": 3
      },
      {
        "id": 7,
        "name": "Kaskazini Pemba",
        "zone": 2
      },
      {
        "id": 8,
        "name": "Kaskazini Unguja",
        "zone": 2
      },
      {
        "id": 9,
        "name": "Katavi",
        "zone": 6
      },
      {
        "id": 10,
        "name": "Kigoma",
        "zone": 3
      },
      {
        "id": 11,
        "name": "Kilimanjaro",
        "zone": 4
      },
      {
        "id": 12,
        "name": "Kusini Pemba",
        "zone": 2
      },
      {
        "id": 13,
        "name": "Kusini Unguja",
        "zone": 2
      },
      {
        "id": 14,
        "name": "Lindi",
        "zone": 5
      },
      {
        "id": 15,
        "name": "Manyara",
        "zone": 4
      },
      {
        "id": 16,
        "name": "Mara",
        "zone": 3
      },
      {
        "id": 17,
        "name": "Mbeya",
        "zone": 6
      },
      {
        "id": 18,
        "name": "Mjini Magharibi",
        "zone": 2
      },
      {
        "id": 19,
        "name": "Morogoro",
        "zone": 2
      },
      {
        "id": 20,
        "name": "Mtwara",
        "zone": 5
      },
      {
        "id": 21,
        "name": "Mwanza",
        "zone": 3
      },
      {
        "id": 22,
        "name": "Njombe",
        "zone": 6
      },
      {
        "id": 23,
        "name": "Pwani",
        "zone": 2
      },
      {
        "id": 24,
        "name": "Rukwa",
        "zone": 6
      },
      {
        "id": 25,
        "name": "Ruvuma",
        "zone": 5
      },
      {
        "id": 26,
        "name": "Shinyanga",
        "zone": 3
      },
      {
        "id": 27,
        "name": "Simiyu",
        "zone": 3
      },
      {
        "id": 28,
        "name": "Singida",
        "zone": 1
      },
      {
        "id": 29,
        "name": "Tabora",
        "zone": 3
      },
      {
        "id": 30,
        "name": "Tanga",
        "zone": 2
      }
    ]
  }
};
Map<String, dynamic> DISTRICTS = {

};
